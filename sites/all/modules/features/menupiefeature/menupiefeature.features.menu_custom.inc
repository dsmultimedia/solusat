<?php
/**
 * @file
 * menupiefeature.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function menupiefeature_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-menu-pie.
  $menus['menu-menu-pie'] = array(
    'menu_name' => 'menu-menu-pie',
    'title' => 'Menu Pie',
    'description' => 'Menú para el pie.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Menu Pie');
  t('Menú para el pie.');


  return $menus;
}
